package com.zuehlke.imdb.client;

import java.text.MessageFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import com.zuehlke.imdb.client.common.table.factory.AbstractTableModelFactory;
import com.zuehlke.imdb.client.common.table.model.GenericTableCell;
import com.zuehlke.imdb.client.common.table.model.GenericTableColumn;
import com.zuehlke.imdb.domain.entities.Movie;

public class ScoredMoviesTableModelFactory extends AbstractTableModelFactory<Movie> {

	private static final String COLUMN_MOVIE_TITLE = "COLUMN_MOVIE_TITLE";
	private static final String COLUMN_MOVIE_YEAR = "COLUMN_MOVIE_YEAR";
	private static final String COLUMN_MOVIE_WEIGHTED_SCORE = "COLUMN_MOVIE_WEIGHTED_SCORE";

	@Override
	protected List<GenericTableColumn> createColumns() {
		List<GenericTableColumn> columns = new ArrayList<>(3);

		columns.add(new GenericTableColumn(COLUMN_MOVIE_TITLE, "Filmtitel"));
		columns.add(new GenericTableColumn(COLUMN_MOVIE_YEAR, "Filmjahr", 60));
		columns.add(new GenericTableColumn(COLUMN_MOVIE_WEIGHTED_SCORE, "Rank", 50, NumberFormat.getIntegerInstance()));

		return columns;
	}

	@Override
	protected GenericTableCell createNonEmptyCell(GenericTableColumn column, Movie movie) {
		switch (column.getId()) {
		case COLUMN_MOVIE_TITLE:
			return new GenericTableCell(column.getId(), movie.getTitle());
		case COLUMN_MOVIE_YEAR:
			return new GenericTableCell(column.getId(), movie.getYear());
		case COLUMN_MOVIE_WEIGHTED_SCORE:
			return new GenericTableCell(column.getId(), movie.getWeightedScore());
		default:
			throw new IllegalStateException(MessageFormat.format(
					"The column with id ''{0}'' is not supported!",
					column.getId()));
		}
	}
}
