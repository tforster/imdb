package com.zuehlke.imdb.client.common.table.factory;

import java.util.Collection;
import java.util.Collections;

public abstract class AbstractTableModelFactory<T> extends
		AbstractGenericTableModelFactory<T> {

	@Override
	protected Collection<T> getChildren(T data) {
		return Collections.emptyList();
	}
}
