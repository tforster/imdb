package com.zuehlke.imdb.client;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.util.Duration;

import org.controlsfx.control.action.Action;
import org.controlsfx.dialog.Dialog;
import org.controlsfx.dialog.Dialogs;

import com.zuehlke.imdb.client.util.StageProvider;

public class MainController implements Initializable {

	@FXML
	private MenuItem statisticsMovieRatings;
	
	@FXML
	private ScoredMoviesController scoredMoviesController;

	@FXML
	private SearchMoviesController searchMoviesController;
	
	@FXML
	private Label versionInfo;
	
	@FXML
	private Label memoryUsageLabel;
	
	@FXML
	private ProgressBar memoryUsageBar;
	
	private StageProvider stageProvider;

	@FXML
	public void handleClose() {
		Action response = Dialogs.create().owner(stageProvider.getStage())
				.title("Beenden")
				.message("Wollen Sie den IMDB Analyzer wirklich beenden?")
				.actions(Dialog.Actions.YES, Dialog.Actions.NO).showConfirm();

		if (response == Dialog.Actions.YES) {
			Platform.exit();
		}
	}
	
	@FXML
	private void handleMovieRatings() {
		showCloseableModal("Filmratings",
				"Anzahl Filme pro Rating (gerundet auf 0.5 Schritte)",
				"/fxml/imdb-statistics-movieratings.fxml");
	}

	@FXML
	private void handleActor2MovieRatings() {
		showCloseableModal("Statistiken - Schauspielerfilmratings", "Durchschnittsfilmrating per Schauspieler im Jarh", "/fxml/imdb-statistics-actor2movieratings.fxml");
	}
	
 	@FXML
 	private void handleAbout() {
		showCloseableModal("�ber...", "Einige Information �ber diese coole App", "/fxml/imdb-about.fxml");
 	}
	
	private void showCloseableModal(String title, String description, String fxml) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(MainApp.class.getResource(fxml));
			Dialog dialog = new Dialog(stageProvider.getStage(), title);
			dialog.setMasthead(description);
			dialog.setContent((Node) loader.load());
			dialog.getContent().setStyle("-fx-padding:0");
			dialog.getActions().add(Dialog.Actions.CLOSE);
			ImageView imageIcon = new ImageView(MainApp.class.getResource("/images/imdb.png").toExternalForm());
			imageIcon.setFitHeight(48.0);
			imageIcon.setFitWidth(48.0);
			dialog.setGraphic(imageIcon);
			dialog.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void setStageProvider(StageProvider stageProvider) {
		this.stageProvider = stageProvider;
		
		this.searchMoviesController.setStageProvider(stageProvider);
		this.scoredMoviesController.setStageProvider(stageProvider);
	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		versionInfo.setText("v0.1");
		Timeline timer = new Timeline(new KeyFrame(Duration.seconds(2), event -> {
			updateMemoryUsage();
		}));
		timer.setCycleCount(Timeline.INDEFINITE);
		timer.play();
		updateMemoryUsage();
	}

	private void updateMemoryUsage() {
		long freeMemory = Runtime.getRuntime().freeMemory() / 1024 / 1024;
		long maxMemory = Runtime.getRuntime().totalMemory() / 1024 / 1024;
		long usedMemory = maxMemory - freeMemory;
		memoryUsageLabel.setText(usedMemory + " / " + maxMemory + " MB");
		memoryUsageBar.setProgress((double) usedMemory / (double) maxMemory);
	}
}
