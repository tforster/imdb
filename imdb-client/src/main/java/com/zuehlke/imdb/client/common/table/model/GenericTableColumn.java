package com.zuehlke.imdb.client.common.table.model;

import java.text.Format;

public class GenericTableColumn {

	private String id;
	private String name;
	private int width;
	private Format formatter;
	
	public GenericTableColumn(String id, String name) {
		this(id, name, null);
	}
	
	public GenericTableColumn(String id, String name, int width) {
		this(id, name, width, null);
	}
	
	public GenericTableColumn(String id, String name, Format formatter) {
		this.id = id;
		this.name = name;
		this.formatter = formatter;
	}
	
	public GenericTableColumn(String id, String name, int width, Format formatter) {
		this.id = id;
		this.name = name;
		this.width = width;
		this.formatter = formatter;
	}

	public String getId() {
		return id;
	}
	
	public String getName() {
		return name;
	}
	
	public int getWidth() {
		return width;
	}

	public Format getFormatter() {
		return formatter;
	}
}
