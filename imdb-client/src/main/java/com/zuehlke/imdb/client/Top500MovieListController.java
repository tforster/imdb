package com.zuehlke.imdb.client;

import static java.time.temporal.ChronoField.DAY_OF_MONTH;
import static java.time.temporal.ChronoField.MONTH_OF_YEAR;
import static java.time.temporal.ChronoField.YEAR;

import java.net.URL;
import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ChoiceBox;

import com.zuehlke.imdb.client.registry.ServiceRegistry;
import com.zuehlke.imdb.domain.entities.BusinessInformation;
import com.zuehlke.imdb.domain.entities.Money;
import com.zuehlke.imdb.domain.entities.Movie;
import com.zuehlke.imdb.domain.entities.RevenueInformation;
import com.zuehlke.imdb.service.movie.RatedMovieService;

public class Top500MovieListController implements Initializable {
	
	private final static String OPTION_MOST_EXPENSIVE = "teuerste Produktion (US Dollar)";
	private final static String OPTION_LONGEST_PRODUCTION = "l�ngste Produktion";
	private final static String OPTION_HIGHEST_RELATIVE_PRODUCTION_COSTS = "h�chste Produktionskosten pro Produktionstag";
	private final static String OPTION_LOWEST_RELATIVE_PRODUCTION_COSTS = "tiefste Produktionskosten pro Produktionstag";
	private final static String OPTION_HIGHEST_REVENUE = "h�chsten Gesamteinspielergebnis (USA)";
	private final static String OPTION_BEST_OPENING_WEEKEND = "bestes Einspielergebnis Premierenwochenende (USA)";
	private final static String OPTION_BEST_MARGIN_ABSOLUTE = "bestes Differenz zwischen Kosten und Einspielergebnis (USA)";
	private final static String OPTION_BEST_MARGIN_RELATIVE = "bestes Verh�ltnis zwischen Kosten und Einspielergebnis (USA)";
	private final static String OPTION_WORST_MARGIN_ABSOLUTE = "schlechteste Differenz zwischen Kosten und Einspielergebnis (USA)";
	private final static String OPTION_WORST_MARGIN_RELATIVE = "schlechtestes Verh�ltnis zwischen Kosten und Einspielergebnis (USA)";
	private final static String OPTION_MOST_ADMISSIONS = "meistbesucht";
	private final static String OPTION_HIGHEST_SCORE = "beste gewichtete Wertung";
	private final static String OPTION_MOST_RATED = "meistbewertet";

	@FXML
	private ChoiceBox<String> criteriaSelector;

	@FXML
	private ResultController resultController;

	private RatedMovieService ratedMovieService;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.ratedMovieService = ServiceRegistry
				.getService(RatedMovieService.class);
		ObservableList<String> options = FXCollections.observableArrayList();
		options.addAll(OPTION_MOST_EXPENSIVE, OPTION_LONGEST_PRODUCTION, OPTION_HIGHEST_RELATIVE_PRODUCTION_COSTS, OPTION_LOWEST_RELATIVE_PRODUCTION_COSTS , OPTION_HIGHEST_REVENUE, OPTION_BEST_OPENING_WEEKEND, OPTION_BEST_MARGIN_ABSOLUTE, OPTION_BEST_MARGIN_RELATIVE, OPTION_WORST_MARGIN_ABSOLUTE, OPTION_WORST_MARGIN_RELATIVE, OPTION_MOST_ADMISSIONS, OPTION_HIGHEST_SCORE, OPTION_MOST_RATED);
		criteriaSelector.setItems(options);
		criteriaSelector.valueProperty().addListener(selectedOption -> updateList());
	}

	@FXML
	public void updateList() {
		updateList(criteriaSelector.valueProperty().get());
	}

	public void updateList(String selectedValue) {
		List<Movie> movies = this.ratedMovieService.getRatedMovies();
		Stream<Movie> results = movies.stream();
		
		switch (selectedValue) {
			case OPTION_LONGEST_PRODUCTION:
				results = results.filter(m -> m.getBusiness() != null && m.getBusiness().getProductionPeriod() != null && m.getBusiness().getProductionPeriod().getProductionStart() != null && m.getBusiness().getProductionPeriod().getProductionEnd() != null).sorted((m1, m2) -> { 
					return computeApproximateProductionDays(m2) - computeApproximateProductionDays(m1);
				});
				break;
			case OPTION_HIGHEST_RELATIVE_PRODUCTION_COSTS:
				results = results.filter(m -> m.getBusiness() != null 
				&& m.getBusiness().getProductionPeriod() != null && m.getBusiness().getProductionPeriod().getProductionStart() != null && m.getBusiness().getProductionPeriod().getProductionEnd() != null && computeApproximateProductionDays(m) > 10
				&& m.getBusiness().getProductionBudget() != null && m.getBusiness().getProductionBudget().getCurrency().equals("USD")  && m.getBusiness().getProductionBudget().getValue() > 0)
				.sorted((m1, m2) -> { 
					return Double.compare(computeApproximateCostsPerProductionDay(m2), computeApproximateCostsPerProductionDay(m1));
				});
				break;
			case OPTION_LOWEST_RELATIVE_PRODUCTION_COSTS:
				results = results.filter(m -> m.getBusiness() != null 
				&& m.getBusiness().getProductionPeriod() != null && m.getBusiness().getProductionPeriod().getProductionStart() != null && m.getBusiness().getProductionPeriod().getProductionEnd() != null && computeApproximateProductionDays(m) > 10
				&& m.getBusiness().getProductionBudget() != null && m.getBusiness().getProductionBudget().getCurrency().equals("USD")  && m.getBusiness().getProductionBudget().getValue() > 0)
				.sorted((m1, m2) -> { 
					return Double.compare(computeApproximateCostsPerProductionDay(m1), computeApproximateCostsPerProductionDay(m2));
				});
				break;
			case OPTION_MOST_EXPENSIVE:
				results = results.filter(m -> m.getBusiness() != null && m.getBusiness().getProductionBudget() != null && m.getBusiness().getProductionBudget().getCurrency().equals("USD")).sorted((m1, m2) -> Money.compareIncreasing(m2.getBusiness().getProductionBudget(), m1.getBusiness().getProductionBudget()));
				break;
			case OPTION_HIGHEST_REVENUE:
				results = results.filter(m -> m.getBusiness() != null).sorted((m1, m2) -> RevenueInformation.maxRevenueComparator().compare(m2.getBusiness().getGrossRevenue(), m1.getBusiness().getGrossRevenue()));
				break;
			case OPTION_BEST_MARGIN_ABSOLUTE:
				results = results.filter(m -> m.getBusiness() != null 
											&& m.getBusiness().getGrossRevenue() != null && m.getBusiness().getGrossRevenue().getMoney().getCurrency().equals("USD") 
											&& m.getBusiness().getProductionBudget() != null  && m.getBusiness().getProductionBudget().getCurrency().equals("USD"))
											.sorted((m1, m2) -> 
												Long.compare(computeAbsoluteProfit(m2.getBusiness()), computeAbsoluteProfit(m1.getBusiness())));
				break;
			case OPTION_BEST_MARGIN_RELATIVE:
				results = results.filter(m -> m.getBusiness() != null 
											&& m.getBusiness().getGrossRevenue() != null && m.getBusiness().getGrossRevenue().getMoney().getCurrency().equals("USD") 
											&& m.getBusiness().getProductionBudget() != null  && m.getBusiness().getProductionBudget().getCurrency().equals("USD") && m.getBusiness().getProductionBudget().getValue() > 0)
											.sorted((m1, m2) -> 
												Double.compare(computeRelativeProfit(m2.getBusiness()), computeRelativeProfit(m1.getBusiness())));
				break;
			case OPTION_WORST_MARGIN_ABSOLUTE:
				results = results.filter(m -> m.getBusiness() != null 
											&& m.getBusiness().getGrossRevenue() != null && m.getBusiness().getGrossRevenue().getMoney().getCurrency().equals("USD") 
											&& m.getBusiness().getProductionBudget() != null  && m.getBusiness().getProductionBudget().getCurrency().equals("USD"))
											.sorted((m1, m2) -> 
												Long.compare(computeAbsoluteProfit(m1.getBusiness()), computeAbsoluteProfit(m2.getBusiness())));
				break;
			case OPTION_WORST_MARGIN_RELATIVE:
				results = results.filter(m -> m.getBusiness() != null 
											&& m.getBusiness().getGrossRevenue() != null && m.getBusiness().getGrossRevenue().getMoney().getCurrency().equals("USD") 
											&& m.getBusiness().getProductionBudget() != null  && m.getBusiness().getProductionBudget().getCurrency().equals("USD") && m.getBusiness().getProductionBudget().getValue() > 0)
											.sorted((m1, m2) -> 
												Double.compare(computeRelativeProfit(m1.getBusiness()), computeRelativeProfit(m2.getBusiness())));
				break;
			case OPTION_BEST_OPENING_WEEKEND:
				results = results.filter(m -> m.getBusiness() != null).sorted((m1, m2) -> RevenueInformation.maxRevenueComparator().compare(m2.getBusiness().getOpeningWeekend(), m1.getBusiness().getOpeningWeekend()));
				break;
			case OPTION_MOST_ADMISSIONS:
				results = results.filter(m -> m.getBusiness() != null).sorted((m1, m2) -> Integer.compare(m2.getBusiness().getAllCountryAdmissions(), m1.getBusiness().getAllCountryAdmissions()));
				break;
			case OPTION_HIGHEST_SCORE: 
				results = results.sorted((m1, m2) -> Double.compare(m2.getWeightedScore(), m1.getWeightedScore()));
				break;
			case OPTION_MOST_RATED: 
				results = results.sorted((m1, m2) -> Integer.compare(m2.getVotes(), m1.getVotes()));
				break;
			default:
		}
		
		results = results.limit(500);
		movies = results.collect(Collectors.toCollection(ArrayList::new));
		resultController.displayRatedMovies(movies);
	}


	private int computeApproximateProductionDays(Movie movie) {
		LocalDate startM1 = toLocalDate(movie.getBusiness().getProductionPeriod().getProductionStart(), false);
		LocalDate endM1 = toLocalDate(movie.getBusiness().getProductionPeriod().getProductionEnd(), true);
		Period p = Period.between(startM1, endM1);
		return p.getYears() * 365 + p.getMonths() * 30 + p.getDays();
	}

	private double computeApproximateCostsPerProductionDay(Movie movie){
		return movie.getBusiness().getProductionBudget().getValue() / (double) computeApproximateProductionDays(movie);
	}

	private long computeAbsoluteProfit(BusinessInformation businessInfo) {
		long revenue = businessInfo.getGrossRevenue().getMoney().getValue();
		long budget = businessInfo.getProductionBudget().getValue();
		return revenue - budget;
	}

	private double computeRelativeProfit(BusinessInformation businessInfo) {
		double revenue = businessInfo.getGrossRevenue().getMoney().getValue();
		double budget = businessInfo.getProductionBudget().getValue();
		return (revenue - budget) / budget;
	}

	private LocalDate toLocalDate(TemporalAccessor info,
			boolean guessHighPartsIfMissing) {
		int year = info.get(YEAR);
		int month = 1;
		int day = 1;
		if (info.isSupported(MONTH_OF_YEAR)) {
			month = info.get(MONTH_OF_YEAR);
		} else if (guessHighPartsIfMissing) {
			year++;
		}
		if (info.isSupported(DAY_OF_MONTH)) {
			day = info.get(DAY_OF_MONTH);
		} else if (guessHighPartsIfMissing) {
			month++;
			while (month > 12) {
				year++;
				month = month -12;
			}
		}
		return LocalDate.of(year, month, day);
	}

}
