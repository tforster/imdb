package com.zuehlke.imdb.client;

import java.text.NumberFormat;
import java.util.List;

import com.zuehlke.imdb.client.common.table.model.GenericTableCell;
import com.zuehlke.imdb.client.common.table.model.GenericTableColumn;
import com.zuehlke.imdb.domain.entities.BusinessInformation;
import com.zuehlke.imdb.domain.entities.Money;
import com.zuehlke.imdb.domain.entities.Movie;

public class RatedMoviesTableModelFactory extends MoviesTableModelFactory {

	protected static final String COLUMN_MOVIE_RANK = "COLUMN_MOVIE_RANK";
	protected static final String COLUMN_MOVIE_VOTES = "COLUMN_MOVIE_VOTES";
	protected static final String COLUMN_MOVIE_BUDGET = "COLUMN_MOVIE_BUDGET";
	protected static final String COLUMN_MOVIE_ADMISSIONS = "COLUMN_MOVIE_ADMISSIONS";
	protected static final String COLUMN_MOVIE_GROSS_REVENUE = "COLUMN_MOVIE_GROSS_REVENUE";
	protected static final String COLUMN_MOVIE_OPENING_WEEKEND = "COLUMN_MOVIE_OPENING_REVENUE";
	
	@Override
	protected List<GenericTableColumn> createColumns() {
		List<GenericTableColumn> columns = super.createColumns();

		columns.add(new GenericTableColumn(COLUMN_MOVIE_RANK, "Rank", 40,
				NumberFormat.getNumberInstance()));
		columns.add(new GenericTableColumn(COLUMN_MOVIE_VOTES,
				"Anzahl Bewertungen", 100, NumberFormat.getIntegerInstance()));
		columns.add(new GenericTableColumn(COLUMN_MOVIE_BUDGET, "Budget"));
		columns.add(new GenericTableColumn(COLUMN_MOVIE_ADMISSIONS,
				"Anzahl Besucher", NumberFormat.getIntegerInstance()));
		columns.add(new GenericTableColumn(COLUMN_MOVIE_GROSS_REVENUE,
				"Gesamteinspielergebnis (USA)"));
		columns.add(new GenericTableColumn(COLUMN_MOVIE_OPENING_WEEKEND,
				"Opening Weekend (USA)"));

		return columns;
	}

	@Override
	protected GenericTableCell createNonEmptyCell(GenericTableColumn column,
			Movie movie) {
		BusinessInformation business = movie.getBusiness();
		switch (column.getId()) {
		case COLUMN_MOVIE_RANK:
			return new GenericTableCell(column.getId(), movie.getRanking());
		case COLUMN_MOVIE_VOTES:
			return new GenericTableCell(column.getId(), movie.getVotes());
		case COLUMN_MOVIE_BUDGET:
			Money budget = new Money();
			if (business != null
					&& business.getProductionBudget() != null) {
				budget = business.getProductionBudget();
			}
			return new GenericTableCell(column.getId(), budget);
		case COLUMN_MOVIE_ADMISSIONS:
			Integer admissions = null;
			if (business != null) {
				admissions = business.getAllCountryAdmissions();
			}
			return new GenericTableCell(column.getId(), admissions);
		case COLUMN_MOVIE_GROSS_REVENUE:
			Money grossRevenue = new Money();
			if (business != null
					&& business.getGrossRevenue() != null) {
			 grossRevenue = business.getGrossRevenue().getMoney();
			}
			return new GenericTableCell(column.getId(), grossRevenue);
		case COLUMN_MOVIE_OPENING_WEEKEND:
			Money openingWeekend = new Money();
			if (business != null
					&& business.getOpeningWeekend() != null) {
				openingWeekend = business.getOpeningWeekend().getMoney();
			}
			return new GenericTableCell(column.getId(), openingWeekend);
		default:
			return super.createNonEmptyCell(column, movie);
		}
	}
}
