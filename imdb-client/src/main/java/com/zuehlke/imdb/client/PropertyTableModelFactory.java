package com.zuehlke.imdb.client;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import com.zuehlke.imdb.client.common.table.factory.AbstractTableModelFactory;
import com.zuehlke.imdb.client.common.table.model.GenericTableCell;
import com.zuehlke.imdb.client.common.table.model.GenericTableColumn;

public class PropertyTableModelFactory extends AbstractTableModelFactory<Property> {
	
	private static final String COLUMN_KEY = "COLUMN_KEY";
	private static final String COLUMN_VALUE = "COLUMN_VALUE";

	@Override
	protected List<GenericTableColumn> createColumns() {
		List<GenericTableColumn> columns = new ArrayList<>(2);

		columns.add(new GenericTableColumn(COLUMN_KEY, "Key"));
		columns.add(new GenericTableColumn(COLUMN_VALUE, "Value"));

		return columns;
	}

	@Override
	protected GenericTableCell createNonEmptyCell(GenericTableColumn column, Property data) {
		switch (column.getId()) {
		case COLUMN_KEY:
			return new GenericTableCell(column.getId(), data.getKey());
		case COLUMN_VALUE:
			return new GenericTableCell(column.getId(), data.getValue());
		default:
			throw new IllegalStateException(MessageFormat.format(
					"The column with id ''{0}'' is not supported!",
					column.getId()));
		}
	}

}
