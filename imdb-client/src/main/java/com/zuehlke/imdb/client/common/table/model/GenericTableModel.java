package com.zuehlke.imdb.client.common.table.model;

import java.util.ArrayList;
import java.util.List;

public class GenericTableModel  {

	private List<GenericTableColumn> columns;
	private List<GenericTableRow> rows;
	
	public GenericTableModel(List<GenericTableColumn> columns) {
		this.columns = columns;
		this.rows = new ArrayList<>();
	}
	
	public void addRow(GenericTableRow row) {
		this.rows.add(row);
	}
	
	public List<GenericTableColumn> getColumns() {
		return columns;
	}
	
	public List<GenericTableRow> getRows() {
		return rows;
	}
}
