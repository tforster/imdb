package com.zuehlke.imdb.client.common.table.model;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class GenericTableRow {

	private Map<String, GenericTableCell> cells;
	private List<GenericTableRow> children;
	
	public GenericTableRow() {
		this.cells = new LinkedHashMap<>();
		this.children = new ArrayList<>();
	}
	
	public void addCell(GenericTableCell cell) {
		this.cells.put(cell.getColumnId(), cell);
	}
	
	public GenericTableCell getCell(String columnId) {
		return cells.get(columnId);
	}
	
	public void addChild(GenericTableRow child) {
		this.children.add(child);
	}
	
	public List<GenericTableRow> getChildren() {
		return children;
	}
}
