package com.zuehlke.imdb.client;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import com.zuehlke.imdb.client.common.table.factory.AbstractTreeTableModelFactory;
import com.zuehlke.imdb.client.common.table.model.GenericTableCell;
import com.zuehlke.imdb.client.common.table.model.GenericTableColumn;
import com.zuehlke.imdb.domain.Actors;
import com.zuehlke.imdb.domain.Genres;
import com.zuehlke.imdb.domain.entities.Movie;

public class MoviesTreeTableModelFactory extends
		AbstractTreeTableModelFactory {

	private static final String COLUMN_NAME = "COLUMN_NAME";
	private static final String COLUMN_GENRE = "COLUMN_GENRE";
	private static final String COLUMN_SEX = "COLUMN_SEX";

	@Override
	protected List<GenericTableColumn> createColumns() {
		List<GenericTableColumn> columns = new ArrayList<>(3);

		columns.add(new GenericTableColumn(COLUMN_NAME, "Name"));
		columns.add(new GenericTableColumn(COLUMN_GENRE, "Genre", 50));
		columns.add(new GenericTableColumn(COLUMN_SEX, "Geschlecht", 100));

		return columns;
	}

	@Override
	protected GenericTableCell createNonEmptyCell(GenericTableColumn column,
			Object data) {

		if (data instanceof Movie) {
			return createCell(column, (Movie) data);
		} else if (data instanceof Actors) {
			return createCell(column, (Actors) data);
		}

		return null;
	}

	private GenericTableCell createCell(GenericTableColumn column, Movie movie) {
		switch (column.getId()) {
		case COLUMN_NAME:
			return new GenericTableCell(column.getId(), movie.getTitle());
		case COLUMN_GENRE:
			String genres = movie.getGenres().stream().map((Genres genre) -> genre.getId().getGenre()).collect(Collectors.joining(", "));
			return new GenericTableCell(column.getId(), genres);
		default:
			return createEmptyCell(column);
		}
	}
	
	private GenericTableCell createCell(GenericTableColumn column, Actors actor) {
		switch (column.getId()) {
		case COLUMN_NAME:
			return new GenericTableCell(column.getId(), actor.getName());
		case COLUMN_SEX:
			return new GenericTableCell(column.getId(), actor.getSex());
		default:
			return createEmptyCell(column);
		}
	}

	@Override
	protected Collection<?> getChildren(Object data) {
		if (data instanceof Movie) {
			return ((Movie) data).getActors();
		}

		return Collections.emptyList();
	}
}
