package com.zuehlke.imdb.client.common.table;

import java.text.Format;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableView;

import com.zuehlke.imdb.client.common.table.model.GenericTableColumn;
import com.zuehlke.imdb.client.common.table.model.GenericTableModel;
import com.zuehlke.imdb.client.common.table.model.GenericTableRow;

public class TableViewUtil {

	public static void initializeTable(TableView<GenericTableRow> tableView,
			GenericTableModel model) {
		for (GenericTableColumn col : model.getColumns()) {
			TableColumn<GenericTableRow, Object> column = new TableColumn<>(
					col.getName());

			if (col.getWidth() > 0) {
				column.setMinWidth(col.getWidth());
			}

			column.setCellValueFactory((
					CellDataFeatures<GenericTableRow, Object> param) -> {
				return param.getValue().getCell(col.getId()).valueProperty();
			});

			setCellFormatter(col.getFormatter(), column);

			tableView.getColumns().add(column);
		}

		ObservableList<GenericTableRow> items = FXCollections.observableList(model.getRows());
		tableView.setItems(items);
		tableView.layout();
	}

	private static void setCellFormatter(Format formatter,
			TableColumn<GenericTableRow, Object> column) {
		if (formatter != null) {
			column.setCellFactory((TableColumn<GenericTableRow, Object> param) -> {
				return new TableCell<GenericTableRow, Object>() {
					@Override
					protected void updateItem(Object item, boolean empty) {
						super.updateItem(item, empty);

						if (item == null || empty) {
							setText(null);
						} else {
							setText(formatter.format(item));
						}
					}
				};
			});
		}
	}

	public static void clearTable(TableView<?> tableView) {
		if (tableView.getColumns() != null) {
			tableView.getColumns().clear();
		}
		
		tableView.setItems(null);
		tableView.layout();
	}
}
