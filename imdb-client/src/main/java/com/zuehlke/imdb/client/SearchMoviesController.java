package com.zuehlke.imdb.client;

import java.net.URL;
import java.util.Collections;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.control.TextField;

import org.apache.commons.lang3.Range;
import org.apache.commons.lang3.StringUtils;
import org.controlsfx.control.RangeSlider;
import org.controlsfx.dialog.Dialogs;

import com.zuehlke.imdb.client.registry.ServiceRegistry;
import com.zuehlke.imdb.client.util.StageProvider;
import com.zuehlke.imdb.domain.entities.Movie;
import com.zuehlke.imdb.service.movie.MovieService;
import com.zuehlke.imdb.service.movie.RatedMovieService;

public class SearchMoviesController implements Initializable {

	@FXML
	private TextField movieNameTextField;

	@FXML
	private RangeSlider ratingRangeSlider;

	@FXML
	private TabPane tablesTabPane;

	@FXML
	private Tab searchResultTab;

	@FXML
	private Tab movieDetailsTab;

	@FXML
	private ResultController resultController;

	@FXML
	private MoviesController moviesController;

	private MovieService movieService;
	private RatedMovieService ratedMovieService;

	private StageProvider stageProvider;

	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.movieService = ServiceRegistry.getService(MovieService.class);
		this.ratedMovieService = ServiceRegistry
				.getService(RatedMovieService.class);

		this.ratingRangeSlider.setMax(10.0);
		this.ratingRangeSlider.setLowValue(1.0);
		this.ratingRangeSlider.setHighValue(10.0);
		this.ratingRangeSlider.setMin(1.0);
		this.ratingRangeSlider.setBlockIncrement(1.0);
		this.ratingRangeSlider.setMajorTickUnit(1.0);
		this.ratingRangeSlider.setMinorTickCount(0);
		this.ratingRangeSlider.setShowTickLabels(true);
		this.ratingRangeSlider.setShowTickMarks(true);
	}

	@FXML
	public void searchMovie() {
		final String movieTitle = (StringUtils.isNotBlank(movieNameTextField
				.getText()) ? movieNameTextField.getText() : null);

		double minRating = ratingRangeSlider.getLowValue();
		double maxRating = ratingRangeSlider.getHighValue();
		final Range<Double> ratingRange = Range.between(minRating, maxRating);

		startProgress("Filme laden", new Service<Void>() {
			@Override
			protected Task<Void> createTask() {
				return new Task<Void>() {
					@Override
					protected Void call() throws Exception {
						updateMessage("Filme werden geladen...");
						updateProgress(0, 3);

						List<Movie> ratedMovies = ratedMovieService
								.getRatedMovies(movieTitle, ratingRange);
						updateProgress(1, 3);

						List<Movie> movies = null;
						if (movieTitle != null) {
							movies = movieService.getMoviesByName(movieTitle);
						} else {
							movies = Collections.emptyList();
						}
						updateProgress(2, 3);

						displayMovies(ratedMovies, movies);
						updateProgress(3, 3);

						return null;
					}
				};
			}
		});
	}

	private void displayMovies(List<Movie> ratedMovies, List<Movie> movies) {
		Platform.runLater(() -> {
			resultController.displayRatedMovies(ratedMovies);
			moviesController.displayMovies(movies);

			tablesTabPane.getSelectionModel().select(searchResultTab);
		});
	}

	private void startProgress(String title, Service<?> progressService) {
		Dialogs.create().owner(stageProvider.getStage()).title(title)
				.showWorkerProgress(progressService);

		progressService.start();
	}

	public void setStageProvider(StageProvider stageProvider) {
		this.stageProvider = stageProvider;
	}
}
