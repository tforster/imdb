package com.zuehlke.imdb.client.common.table;

import java.text.Format;
import java.util.Collection;

import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableColumn.CellDataFeatures;
import javafx.scene.control.TreeTableView;
import javafx.util.Callback;

import com.zuehlke.imdb.client.common.table.model.GenericTableColumn;
import com.zuehlke.imdb.client.common.table.model.GenericTableModel;
import com.zuehlke.imdb.client.common.table.model.GenericTableRow;

public class TreeTableViewUtil {

	public static void initializeTable(
			TreeTableView<GenericTableRow> treeTableView,
			GenericTableModel model) {
		for (GenericTableColumn col : model.getColumns()) {
			TreeTableColumn<GenericTableRow, Object> column = new TreeTableColumn<>(
					col.getName());
			
			if (col.getWidth() > 0) {
				column.setMinWidth(col.getWidth());
			}

			column.setCellValueFactory((
					CellDataFeatures<GenericTableRow, Object> param) -> {
				return param.getValue().getValue().getCell(col.getId())
						.valueProperty();
			});

			setCellFormatter(col.getFormatter(), column);

			treeTableView.getColumns().add(column);
		}

		TreeItem<GenericTableRow> root = new TreeItem<>();
		addTreeItems(root, model.getRows());

		treeTableView.setShowRoot(false);
		treeTableView.setRoot(root);
		treeTableView.layout();
	}

	private static void addTreeItems(TreeItem<GenericTableRow> root,
			Collection<GenericTableRow> rows) {
		for (GenericTableRow row : rows) {
			TreeItem<GenericTableRow> treeItem = new TreeItem<GenericTableRow>(
					row);

			addTreeItems(treeItem, row.getChildren());

			root.getChildren().add(treeItem);
		}
	}

	private static void setCellFormatter(Format formatter,
			TreeTableColumn<GenericTableRow, Object> column) {
		if (formatter != null) {
			column.setCellFactory(new Callback<TreeTableColumn<GenericTableRow, Object>, TreeTableCell<GenericTableRow, Object>>() {
				@Override
				public TreeTableCell<GenericTableRow, Object> call(
						TreeTableColumn<GenericTableRow, Object> param) {
					return new TreeTableCell<GenericTableRow, Object>() {
						@Override
						protected void updateItem(Object item, boolean empty) {
							super.updateItem(item, empty);

							if (item == null || empty) {
								setText(null);
							} else {
								setText(formatter.format(item));
							}
						}
					};
				}
			});
		}
	}

	public static void clearTable(TreeTableView<?> treeTableView) {
		if (treeTableView.getColumns() != null) {
			treeTableView.getColumns().clear();
		}
		treeTableView.setRoot(null);
		treeTableView.layout();
	}
}
