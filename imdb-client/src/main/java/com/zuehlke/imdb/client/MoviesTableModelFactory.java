package com.zuehlke.imdb.client;

import static java.time.temporal.ChronoField.*;

import java.text.MessageFormat;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.List;

import com.zuehlke.imdb.client.common.table.factory.AbstractTableModelFactory;
import com.zuehlke.imdb.client.common.table.model.GenericTableCell;
import com.zuehlke.imdb.client.common.table.model.GenericTableColumn;
import com.zuehlke.imdb.domain.entities.BusinessInformation;
import com.zuehlke.imdb.domain.entities.Movie;
import com.zuehlke.imdb.domain.entities.ProductionPeriod;

public class MoviesTableModelFactory extends AbstractTableModelFactory<Movie> {

	private static final String COLUMN_MOVIE_TITLE = "COLUMN_MOVIE_TITLE";
	private static final String COLUMN_MOVIE_YEAR = "COLUMN_MOVIE_YEAR";
	private static final String COLUMN_MOVIE_PRODUCTION_START = "COLUMN_MOVIE_PRODUCTION_START";
	private static final String COLUMN_MOVIE_PRODUCTION_END = "COLUMN_MOVIE_PRODUCTION_END";

	@Override
	protected List<GenericTableColumn> createColumns() {
		List<GenericTableColumn> columns = new ArrayList<>(4);

		DateTimeFormatter formatter = new DateTimeFormatterBuilder()
		.parseCaseInsensitive().optionalStart().appendValue(DAY_OF_MONTH, 2)
		.appendLiteral(".").optionalEnd().optionalStart().appendValue(MONTH_OF_YEAR, 2)
		.appendLiteral(".").optionalEnd().appendValue(YEAR, 4).toFormatter();
		
		columns.add(new GenericTableColumn(COLUMN_MOVIE_TITLE, "Filmtitel"));
		columns.add(new GenericTableColumn(COLUMN_MOVIE_YEAR, "Filmjahr", 60));
		columns.add(new GenericTableColumn(COLUMN_MOVIE_PRODUCTION_START,
				"Produktionsstart", 100, formatter.toFormat()));
		columns.add(new GenericTableColumn(COLUMN_MOVIE_PRODUCTION_END,
				"Produktionsende", 100, formatter.toFormat()));

		return columns;
	}

	@Override
	protected GenericTableCell createNonEmptyCell(GenericTableColumn column,
			Movie movie) {
		switch (column.getId()) {
		case COLUMN_MOVIE_TITLE:
			return new GenericTableCell(column.getId(), movie.getTitle());
		case COLUMN_MOVIE_YEAR:
			return new GenericTableCell(column.getId(), movie.getYear());
		case COLUMN_MOVIE_PRODUCTION_START:
			TemporalAccessor productionStart = null;
			
			if (getProductionPeriod(movie) != null) {
				productionStart = getProductionPeriod(movie).getProductionStart();
			}
			
			return new GenericTableCell(column.getId(), productionStart);
		case COLUMN_MOVIE_PRODUCTION_END:
			TemporalAccessor productionEnd = null;
			
			if (getProductionPeriod(movie) != null) {
				productionEnd = getProductionPeriod(movie).getProductionEnd();
			}
			
			return new GenericTableCell(column.getId(), productionEnd);
		default:
			throw new IllegalStateException(MessageFormat.format(
					"The column with id ''{0}'' is not supported!",
					column.getId()));
		}
	}
	
	private ProductionPeriod getProductionPeriod(Movie movie) {
		BusinessInformation businessInformation = movie.getBusiness();
		if (businessInformation == null) {
			return null;
		}
		
		return businessInformation.getProductionPeriod();
	}
}
