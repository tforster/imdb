package com.zuehlke.imdb.client.registry;

import java.util.HashMap;
import java.util.Map;

public class ServiceRegistry {

	/**
     * Not allowed to instantiate utility class.
     */
    private ServiceRegistry() {
    }

    private static Map<String, Object> registeredServices = new HashMap<>();

    /**
     * Register a service implementation to be used for a service interface.
     * 
     * @param <T> the interface of the service that is registered
     * @param serviceInterfaceClass the service interface class
     * @param serviceImpl the service implementation to be used in the application.
     */

    public static synchronized <T> void register(Class<T> serviceInterfaceClass, T serviceImpl) {
        registeredServices.put(getKey(serviceInterfaceClass), serviceImpl);
    }

    /**
     * Register a service implementation to be used for a service interface.
     * 
     * @param <T> the interface of the service that is registered
     * @param serviceInterfaceClass the service interface class
     * @param postfix a postfix for the registry key
     * @param serviceImpl the service implementation to be used in the application.
     */
    public static synchronized <T> void register(Class<T> serviceInterfaceClass, String postfix, T serviceImpl) {
        registeredServices.put(getKey(serviceInterfaceClass, postfix), serviceImpl);
    }

    /**
     * Unregister an implementation of a service interface.
     * 
     * @param <T> the service interface
     * @param serviceInterfaceClass the service interface class.
     */
    public static synchronized <T> void unregister(Class<T> serviceInterfaceClass) {
        registeredServices.remove(getKey(serviceInterfaceClass));
    }

    /**
     * Unregister an implementation of a service interface.
     * 
     * @param <T> the service interface
     * @param serviceInterfaceClass the service interface class.
     * @param postfix a postfix for the registry key
     */
    public static synchronized <T> void unregister(Class<T> serviceInterfaceClass, String postfix) {
        registeredServices.remove(getKey(serviceInterfaceClass, postfix));
    }

    /**
     * Get the implementation of a service interface to be used.
     * 
     * @param <T> the service interface
     * @param serviceInterfaceClass the service interface class
     * @return the registered implementation of the service
     */
    @SuppressWarnings("unchecked")
    public static synchronized <T> T getService(Class<T> serviceInterfaceClass) {
        return (T) registeredServices.get(getKey(serviceInterfaceClass));
    }

    /**
     * Get the implementation of a service interface to be used.
     * 
     * @param <T> the service interface
     * @param serviceInterfaceClass the service interface class
     * @param postfix a postfix for the registry key
     * @return the registered implementation of the service
     */
    @SuppressWarnings("unchecked")
    public static synchronized <T> T getService(Class<T> serviceInterfaceClass, String postfix) {
        return (T) registeredServices.get(getKey(serviceInterfaceClass, postfix));
    }

    private static String getKey(Class<?> serviceInterfaceClass, String... postfixes) {
        StringBuilder builder = new StringBuilder();

        builder.append(serviceInterfaceClass.getName());
        for (String postfix : postfixes) {
            builder.append("_"); //$NON-NLS-1$
            builder.append(postfix);
        }

        return builder.toString();
    }
}
