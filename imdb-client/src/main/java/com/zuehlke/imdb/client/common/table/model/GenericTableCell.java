package com.zuehlke.imdb.client.common.table.model;

import javafx.beans.property.SimpleObjectProperty;

public class GenericTableCell {

	private String columnId;
	private SimpleObjectProperty<Object> valueProperty;

	public GenericTableCell(String columnId) {
		this.columnId = columnId;
		this.valueProperty = new SimpleObjectProperty<>();
	}

	public GenericTableCell(String columnId, Object value) {
		this.columnId = columnId;
		this.valueProperty = new SimpleObjectProperty<>(value);
	}

	public String getColumnId() {
		return columnId;
	}

	public SimpleObjectProperty<Object> valueProperty() {
		return valueProperty;
	}
}
