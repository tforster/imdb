package com.zuehlke.imdb.client;

import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.control.TreeTableView;

import com.zuehlke.imdb.client.common.table.TreeTableViewUtil;
import com.zuehlke.imdb.client.common.table.model.GenericTableModel;
import com.zuehlke.imdb.client.common.table.model.GenericTableRow;
import com.zuehlke.imdb.domain.entities.Movie;

public class MoviesController {

	@FXML
	private TreeTableView<GenericTableRow> moviesTreeTable;

	public void displayMovies(List<Movie> movies) {
		MoviesTreeTableModelFactory factory = new MoviesTreeTableModelFactory();
		GenericTableModel model = factory.createModel(movies);

		TreeTableViewUtil.clearTable(moviesTreeTable);
		TreeTableViewUtil.initializeTable(moviesTreeTable, model);
	}
}
