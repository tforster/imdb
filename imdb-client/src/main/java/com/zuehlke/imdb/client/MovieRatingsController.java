package com.zuehlke.imdb.client;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.zuehlke.imdb.client.registry.ServiceRegistry;
import com.zuehlke.imdb.domain.entities.Movie;
import com.zuehlke.imdb.service.movie.RatedMovieService;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Tooltip;
import javafx.util.Duration;

public class MovieRatingsController {

	@FXML
	private BarChart<String, Long> barChart;

	@FXML
	private CategoryAxis xAxis;

	@FXML
	private NumberAxis yAxis;

	private ObservableList<XYChart.Series<String, Long>> barChartData;

	@FXML
	private void initialize() {
		Series<String, Long> series = new XYChart.Series<>();
		series.setName("Anzahl Filme");
		
		barChartData = FXCollections.observableArrayList();
		barChartData.add(series);
		barChart.setData(barChartData);
		barChart.setAnimated(true);

		xAxis.setLabel("Rating");
		yAxis.setLabel("Anzahl Filme");

		RatedMovieService ratingsService = ServiceRegistry.getService(RatedMovieService.class);

		List<Movie> ratedMovies = ratingsService.getRatedMovies();

		Map<Double, Long> ratedMoviesMap = ratedMovies.stream()
				.collect(Collectors.groupingBy(
						m -> Math.round(m.getRanking() * 2) / 2.0 ,
						TreeMap::new,
						Collectors.counting()));

		ObservableList<XYChart.Data<String, Long>> data = FXCollections.observableArrayList();

		ratedMoviesMap.forEach((k, v) -> {
			data.add(new XYChart.Data<String, Long>(k + "", v));
		});

		barChartData.get(0).setData(data);

		createTooltips();
	}

	private void createTooltips() {
		setupCustomTooltipBehavior(0, 5000, 2000);

		for (XYChart.Series<String, Long> s : barChart.getData()) {
			for (XYChart.Data<String, Long> d : s.getData()) {
				Tooltip tooltip = new Tooltip(
						String.format("%d Filme", d.getYValue()));
				Tooltip.install(d.getNode(), tooltip);
			}
		}
	}

	/** 
	 * <p> 
	 * Tooltip behavior is controlled by a private class javafx.scene.control.Tooltip$TooltipBehavior. 
	 * All Tooltips share the same TooltipBehavior instance via a static private member BEHAVIOR, which 
	 * has default values of 1sec for opening, 5secs visible, and 200 ms close delay (if mouse exits from node before 5secs).  
	 *  
	 * The hack below constructs a custom instance of TooltipBehavior and replaces private member BEHAVIOR with 
	 * this custom instance. 
	 * </p> 
	 * 
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private void setupCustomTooltipBehavior(int openDelayInMillis, int visibleDurationInMillis, int closeDelayInMillis) {  
		try {  
			Class TTBehaviourClass = null;  
			Class<?>[] declaredClasses = Tooltip.class.getDeclaredClasses();  
			for (Class c:declaredClasses) {  
				if (c.getCanonicalName().equals("javafx.scene.control.Tooltip.TooltipBehavior")) {  
					TTBehaviourClass = c;  
					break;  
				}  
			}  
			if (TTBehaviourClass == null) {  
				// abort  
				return;  
			}  
			Constructor constructor = TTBehaviourClass.getDeclaredConstructor(  
					Duration.class, Duration.class, Duration.class, boolean.class);  
			if (constructor == null) {  
				// abort  
				return;  
			}  
			constructor.setAccessible(true);  
			Object newTTBehaviour = constructor.newInstance(  
					new Duration(openDelayInMillis), new Duration(visibleDurationInMillis),   
					new Duration(closeDelayInMillis), false);  
			if (newTTBehaviour == null) {  
				// abort  
				return;  
			}  
			Field ttbehaviourField = Tooltip.class.getDeclaredField("BEHAVIOR");  
			if (ttbehaviourField == null) {  
				// abort  
				return;  
			}  
			ttbehaviourField.setAccessible(true);  

			// Cache the default behavior if needed.  
			@SuppressWarnings("unused")
			Object defaultTTBehavior = ttbehaviourField.get(Tooltip.class);  
			ttbehaviourField.set(Tooltip.class, newTTBehaviour);  

		} catch (Exception e) {  
			System.out.println("Aborted setup due to error:" + e.getMessage());  
		}  
	} 

}
