package com.zuehlke.imdb.client;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import javafx.fxml.FXML;
import javafx.scene.chart.AreaChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.TextField;

import com.zuehlke.imdb.client.registry.ServiceRegistry;
import com.zuehlke.imdb.domain.entities.Movie;
import com.zuehlke.imdb.service.movie.MovieAggregationService;

public class Actor2MovieRatingsController {

	@FXML
	private AreaChart<Long, Double> areaChart;
	@FXML
	private NumberAxis xAxis;
	@FXML
	private NumberAxis yAxis;

	@FXML
	private TextField actor1;
	
	@FXML
	private TextField actor2;
	
	private MovieAggregationService movieAggrService;
	
	@FXML
	private void initialize() {
		areaChart.setAnimated(true);
		xAxis.setLabel("Jahr");
		yAxis.setLabel("Durchschnittsbewertung");

		movieAggrService = ServiceRegistry.getService(MovieAggregationService.class);		
	}
	
	@FXML
	public void showStatistics(){
		areaChart.getData().clear();
		XYChart.Series<Long, Double> data1 = getAverageScoresInYearByActor(actor1.getText()); // "DiCaprio, Leonardo"
		XYChart.Series<Long, Double> data2 = getAverageScoresInYearByActor(actor2.getText()); //Clooney, George, Cranston, Bryan  Hill, Jonah
		areaChart.getData().addAll(data1, data2);		
	}
	
	private XYChart.Series<Long, Double> getAverageScoresInYearByActor(String name){
		//System.out.println("----------------------------------");
		
		List<Movie> movies = movieAggrService.getMoviesWithWeightedScoresByActorname(name);
		Map<Long, Double> year2AverageScore = movies.stream().
				collect(Collectors.groupingBy(m -> Long.parseLong(m.getYear().replaceAll("[-]{1}.*", "")), TreeMap::new,
												Collectors.averagingDouble(Movie::getWeightedScore)));

		XYChart.Series<Long, Double> data = new XYChart.Series();
		year2AverageScore.forEach((k, v) -> {
			data.getData().add(new XYChart.Data<Long, Double>(k, v));
			//System.out.println(k + ": " + v);
		});
		data.setName(name);
		
		return data;
	}
}
