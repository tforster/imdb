package com.zuehlke.imdb.client.util;

import javafx.stage.Stage;

public interface StageProvider {

	Stage getStage();
}
