package com.zuehlke.imdb.client.common.table.factory;

import java.util.Collection;
import java.util.List;

import com.zuehlke.imdb.client.common.table.model.GenericTableCell;
import com.zuehlke.imdb.client.common.table.model.GenericTableColumn;
import com.zuehlke.imdb.client.common.table.model.GenericTableModel;
import com.zuehlke.imdb.client.common.table.model.GenericTableRow;

abstract class AbstractGenericTableModelFactory<T> {

	public final GenericTableModel createModel(Collection<? extends T> modelData) {
		GenericTableModel model = new GenericTableModel(createColumns());

		createRows(model, modelData);

		return model;
	}

	protected abstract List<GenericTableColumn> createColumns();

	protected final GenericTableCell createEmptyCell(GenericTableColumn column) {
		return new GenericTableCell(column.getId());
	}

	protected abstract GenericTableCell createNonEmptyCell(GenericTableColumn column, T data);

	protected abstract Collection<? extends T> getChildren(T data);

	private GenericTableCell createCell(GenericTableColumn column, T data) {
		if (data == null) {
			return createEmptyCell(column);
		} else {
			return createNonEmptyCell(column, data);
		}

	}
	
	private GenericTableRow createRow(List<GenericTableColumn> columns, T data) {
		GenericTableRow row = new GenericTableRow();
		for (GenericTableColumn column : columns) {
			row.addCell(createCell(column, data));
		}
		
		for (T child : getChildren(data)) {
			row.addChild(createRow(columns, child));
		}

		return row;
	}
	
	private void createRows(GenericTableModel model, Collection<? extends T> modelData) {
		for (T data : modelData) {
			model.addRow(createRow(model.getColumns(), data));
		}
	}
}
