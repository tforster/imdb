package com.zuehlke.imdb.client;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableView;

import com.zuehlke.imdb.client.common.table.TableViewUtil;
import com.zuehlke.imdb.client.common.table.factory.AbstractTableModelFactory;
import com.zuehlke.imdb.client.common.table.model.GenericTableModel;
import com.zuehlke.imdb.client.common.table.model.GenericTableRow;


public class AboutDialogController implements Initializable {
	
	@FXML
	private TableView<GenericTableRow> propertiesTable;

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		
		List<Property> systemProperties = new ArrayList<Property>();
		
		for(Object k : System.getProperties().keySet()) {
			String key = (String) k;
			systemProperties.add(new Property(key, System.getProperty(key)));
		}
		
		displayProperties(systemProperties, new PropertyTableModelFactory());
	}

	public void displayProperties(List<Property> properties, AbstractTableModelFactory<Property> factory) {
		GenericTableModel model = factory.createModel(properties);

		TableViewUtil.clearTable(propertiesTable);
		TableViewUtil.initializeTable(propertiesTable, model);
	}
}
