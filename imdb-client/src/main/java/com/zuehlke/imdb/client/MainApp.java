package com.zuehlke.imdb.client;

import java.sql.SQLException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.zuehlke.imdb.client.init.ApplicationInitializer;
import com.zuehlke.imdb.client.util.StageProvider;

public class MainApp extends Application implements StageProvider {

    private static final Logger log = LoggerFactory.getLogger(MainApp.class);

    public static void main(String[] args) throws Exception {
        launch(args);
    }

    private ApplicationInitializer initializer;
	private Stage stage;

	public MainApp() {
		this.initializer = new ApplicationInitializer();
	}
	
	@Override
	public void init() throws Exception {
		super.init();
		
		this.initializer.initializePersistence();
		this.initializer.initializeServices();
		
		this.initializer.registerShutdownHooks();
	}
	
    public void start(Stage stage) throws Exception {
    	this.stage = stage;
    	
        log.info("Starting IMDB Analyzer");

        String fxmlFile = "/fxml/imdb-main.fxml";
        log.debug("Loading FXML for main view from: {}", fxmlFile);
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/fxml/"));
        Parent rootNode = (Parent) loader.load(getClass().getResourceAsStream(fxmlFile));

        MainController controller = loader.getController();
        controller.setStageProvider(this);

        log.debug("Showing JFX scene");
        Scene scene = new Scene(rootNode);
        scene.getStylesheets().add("/styles/styles.css");

        stage.setTitle("IMDB Analyzer");
        stage.setScene(scene);
        stage.show();
    }
    
    @Override
    public void stop() throws Exception {
    	super.stop();
    	
    	this.initializer.shutdown();
    }
    
    @Override
    public Stage getStage() {
    	return stage;
    }
}
