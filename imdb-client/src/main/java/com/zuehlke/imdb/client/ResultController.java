package com.zuehlke.imdb.client;

import java.util.List;

import javafx.fxml.FXML;
import javafx.scene.control.TableView;

import com.zuehlke.imdb.client.common.table.TableViewUtil;
import com.zuehlke.imdb.client.common.table.model.GenericTableModel;
import com.zuehlke.imdb.client.common.table.model.GenericTableRow;
import com.zuehlke.imdb.domain.entities.Movie;

public class ResultController {

	@FXML
	private TableView<GenericTableRow> resultTable;

	public void displayResults(GenericTableModel model) {
		TableViewUtil.clearTable(resultTable);
		TableViewUtil.initializeTable(resultTable, model);
	}
	
	public void displayMovies(List<Movie> movies) {
		MoviesTableModelFactory factory = new MoviesTableModelFactory();
		GenericTableModel model = factory.createModel(movies);

		displayResults(model);
	}
	
	public void displayRatedMovies(List<Movie> movies) {
		RatedMoviesTableModelFactory factory = new RatedMoviesTableModelFactory();
		GenericTableModel model = factory.createModel(movies);

		displayResults(model);
	}
}
