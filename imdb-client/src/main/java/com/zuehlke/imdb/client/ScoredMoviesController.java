package com.zuehlke.imdb.client;

import java.net.URL;
import java.text.MessageFormat;
import java.util.List;
import java.util.ResourceBundle;

import javafx.application.Platform;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Accordion;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;

import org.apache.commons.lang3.StringUtils;
import org.controlsfx.dialog.Dialogs;

import com.zuehlke.imdb.client.common.table.TableViewUtil;
import com.zuehlke.imdb.client.common.table.factory.AbstractTableModelFactory;
import com.zuehlke.imdb.client.common.table.model.GenericTableModel;
import com.zuehlke.imdb.client.common.table.model.GenericTableRow;
import com.zuehlke.imdb.client.registry.ServiceRegistry;
import com.zuehlke.imdb.client.util.StageProvider;
import com.zuehlke.imdb.domain.entities.Movie;
import com.zuehlke.imdb.service.movie.MovieAggregationService;

public class ScoredMoviesController implements Initializable {
	@FXML
	private TextField movieNameTextField;

	@FXML
	private TextField actorTextField;

	@FXML
	private Accordion accordion;

	@FXML
	private TitledPane movieTitleTitledPane;

	@FXML
	private TitledPane actorTitleTitledPane;

	@FXML
	private TableView<GenericTableRow> resultTable;

	private MovieAggregationService service;

	private StageProvider stageProvider;

	public void displayMovies(List<Movie> movies,
			AbstractTableModelFactory<Movie> factory) {
		GenericTableModel model = factory.createModel(movies);

		TableViewUtil.clearTable(resultTable);
		TableViewUtil.initializeTable(resultTable, model);
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		this.service = ServiceRegistry
				.getService(MovieAggregationService.class);
		this.accordion.setExpandedPane(movieTitleTitledPane);
	}

	@FXML
	public void searchByActor() {
		if (StringUtils.isBlank(actorTextField.getText())) {
			this.actorTextField.getStyleClass().add("error-text-field");
			return;
		}

		this.actorTextField.getStyleClass().remove("error-text-field");

		String actor = actorTextField.getText();
		
		startProgress("Filme laden", new Service<Void>() {
			@Override
			protected Task<Void> createTask() {
				return new Task<Void>() {
					@Override
					protected Void call() throws Exception {
						updateMessage(MessageFormat.format(
								"Filme mit Schauspieler ''{0}'' werden geladen...",
								actor));
						updateProgress(-1, -1);

						List<Movie> movies = service
								.getMoviesWithWeightedScoresByActorname(actor);
						displayMovies(movies);
						
						return null;
					}
				};
			}
		});
	}
	
	
	@FXML
	public void searchByMovieTitle() {
		if (StringUtils.isBlank(movieNameTextField.getText())) {
			this.movieNameTextField.getStyleClass().add("error-text-field");
			return;
		}

		this.movieNameTextField.getStyleClass().remove("error-text-field");

		String movieTitle = movieNameTextField.getText();

		startProgress("Filme laden", new Service<Void>() {
			@Override
			protected Task<Void> createTask() {
				return new Task<Void>() {
					@Override
					protected Void call() throws Exception {
						updateMessage(MessageFormat.format(
								"Filme mit Titel ''{0}'' werden geladen...",
								movieTitle));
						updateProgress(-1, -1);

						List<Movie> movies = service
								.getMoviesWithWeightedScoreByMoviename(movieTitle);
						displayMovies(movies);
						
						return null;
					}
				};
			}
		});
	}

	public void setStageProvider(StageProvider stageProvider) {
		this.stageProvider = stageProvider;
	}

	private void displayMovies(List<Movie> movies) {
		Platform.runLater(() -> displayMovies(movies, new ScoredMoviesTableModelFactory()));
	}

	private void startProgress(String title, Service<?> progressService) {
		Dialogs.create().owner(stageProvider.getStage()).title(title)
		.showWorkerProgress(progressService);

		progressService.start();
	}
}
