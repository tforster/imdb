package com.zuehlke.imdb.client.init;

import static com.zuehlke.imdb.client.util.Constants.PERSISTENCE_UNIT_NAME;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.zuehlke.imdb.client.registry.ServiceRegistry;
import com.zuehlke.imdb.service.movie.MovieAggregationService;
import com.zuehlke.imdb.service.movie.MovieService;
import com.zuehlke.imdb.service.movie.RatedMovieService;
import com.zuehlke.imdb.service.movie.impl.MovieAggregationServiceImpl;
import com.zuehlke.imdb.service.movie.impl.MovieServiceImpl;
import com.zuehlke.imdb.service.movie.impl.RatedMovieServiceImpl;

public class ApplicationInitializer {

	public void initializePersistence() {
		EntityManagerFactory factory = putService(EntityManagerFactory.class,
				Persistence.createEntityManagerFactory(PERSISTENCE_UNIT_NAME));
		putService(EntityManager.class, factory.createEntityManager());
	}

	public void initializeServices() {
		putService(MovieService.class, new MovieServiceImpl(
				getService(EntityManager.class)));
		putService(RatedMovieService.class, new RatedMovieServiceImpl(
				getService(EntityManager.class)));
		putService(MovieAggregationService.class, new MovieAggregationServiceImpl(
				getService(EntityManager.class)));
	}

	public void registerShutdownHooks() {
		final EntityManagerFactory factory = getService(EntityManagerFactory.class);
		final EntityManager entityManager = getService(EntityManager.class);

		Runtime.getRuntime().addShutdownHook(new Thread() {
			public void run() {
				if (entityManager.isOpen()) {
					entityManager.close();
				}

				if (factory.isOpen()) {
					factory.close();
				}
			}
		});
	}

	public void shutdown() {
		getService(EntityManager.class).close();
		getService(EntityManagerFactory.class).close();
	}

	/**
	 * Returns the service for the specified type from the
	 * {@link ServiceRegistry}.
	 * 
	 * @param <T>
	 *            type of service
	 * @param serviceType
	 *            service type
	 * @return the service from the ServiceRegistry
	 */
	private <T> T getService(Class<T> serviceType) {
		T service = ServiceRegistry.getService(serviceType);
		if (service == null) {
			throw new RuntimeException(
					"No Service in ServiceRegistry for service type " + serviceType.getName()); //$NON-NLS-1$
		}
		return service;
	}

	/**
	 * Utility method to store a service in the {@link ServiceRegistry}.
	 * 
	 * @param <T>
	 *            service type
	 * @param serviceType
	 *            service type (or a super type)
	 * @param service
	 *            service
	 * @return the service
	 */
	private <T> T putService(Class<? super T> serviceType, T service) {
		ServiceRegistry.register(serviceType, service);
		return service;
	}

	/**
	 * Utility method to store a service in the {@link ServiceRegistry}. Uses
	 * the service's class as key in the ServiceRegistry.
	 * 
	 * @param <T>
	 *            service type
	 * @param service
	 *            service
	 * @return the service
	 */
	@SuppressWarnings("unchecked")
	private <T> T putService(T service) {
		ServiceRegistry.register((Class<T>) service.getClass(), service);
		return service;
	}
}
