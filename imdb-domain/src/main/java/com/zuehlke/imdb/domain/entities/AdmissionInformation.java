package com.zuehlke.imdb.domain.entities;

import java.util.Comparator;

public class AdmissionInformation extends BusinessInformationDetail {
	private int count;

	public static Comparator<AdmissionInformation> maxCountComparator() {
		return (adm1, adm2) -> Integer.compare(adm1.getCount(),
				adm2.getCount());
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

}
