package com.zuehlke.imdb.domain.entities;

import java.text.NumberFormat;

import org.apache.commons.lang3.StringUtils;

public class Money implements Comparable<Money> {
	private static final String NO_CURRENCY = "(none)";
	final static NumberFormat nf = NumberFormat.getInstance();
	long value = 0;
	String currency = NO_CURRENCY;

	public String getCurrency() {
		return currency;
	}

	public void setCurrency(String currency) {
		this.currency = currency;
	}

	public long getValue() {
		return value;
	}

	public void setValue(long value) {
		this.value = value;
	}

	@Override
	public String toString() {
		if (StringUtils.equals(NO_CURRENCY, getCurrency())) {
			return "";
		} else {
			return currency + " " + nf.format(value);
		}
	}

	@Override
	public int compareTo(Money o) {
		int comparedCurrency = getCurrency().compareTo(o.getCurrency());
		if (comparedCurrency == 0) {
			return Long.compare(getValue(), o.getValue());
		} else { 
			return comparedCurrency;
		}
	}
	
	public static int compareIncreasing(Money m1, Money m2) {
		return m1.compareTo(m2);
	}

	public static int compareDecreasing(Money m1, Money m2) {
		return m2.compareTo(m1);
	}
}
