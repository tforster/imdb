package com.zuehlke.imdb.domain.entities;

import java.time.temporal.TemporalAccessor;
import java.util.Comparator;

public class RevenueInformation extends BusinessInformationDetail {
	Money money = new Money();
	String country;
	TemporalAccessor date;
	int screens;

	public static Comparator<RevenueInformation> maxRevenueComparator() {
		return (rev1, rev2) -> {
		if (rev1 == null || rev1.getMoney() == null) return -1;
		if (rev2 == null || rev2.getMoney() == null) return 1;
		return Long.compare(rev1.getMoney().getValue(),
				rev2.getMoney().getValue());
		};
	}

	public static Comparator<RevenueInformation> maxScreensComparator() {
		return (rev1, rev2) -> Integer.compare(rev1.getScreens(),
				rev2.getScreens());
	}

	public Money getMoney() {
		return money;
	}

	public void setMoney(Money money) {
		this.money = money;
	}

	@Override
	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public TemporalAccessor getDate() {
		return date;
	}

	public void setDate(TemporalAccessor date) {
		this.date = date;
	}

	public int getScreens() {
		return screens;
	}

	public void setScreens(int screens) {
		this.screens = screens;
	}
}
