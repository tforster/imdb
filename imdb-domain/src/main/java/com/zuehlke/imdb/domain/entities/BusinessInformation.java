package com.zuehlke.imdb.domain.entities;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;


public class BusinessInformation {

	/**
	 * default country, for which most information is available in IMDB
	 * business.list = (USA)
	 **/
	public final static String DEFAULT_COUNTRY = "USA";

	private List<String> copyRight = new LinkedList<String>(); // CP

	private Money productionBudget; // BT
	private ProductionPeriod productionPeriod; // PD or SD

	private Map<String, List<AdmissionInformation>> totalAdmissionsByCountry = new HashMap<>(); // AD

	private Map<String, RevenueInformation> openingWeekendByCountry = new HashMap<>(); // OW
	private Map<String, List<RevenueInformation>> weeklyGrossByCountry = new HashMap<>(); // WG
	private Map<String, List<RevenueInformation>> grossRevenueByCountry = new HashMap<>(); // GR

	public List<String> getCopyRight() {
		return copyRight;
	}

	public Money getProductionBudget() {
		return productionBudget;
	}

	public void setProductionBudget(Money productionBudget) {
		this.productionBudget = productionBudget;
	}

	public ProductionPeriod getProductionPeriod() {
		return productionPeriod;
	}

	public void setProductionPeriod(ProductionPeriod productionPeriod) {
		this.productionPeriod = productionPeriod;
	}

	public void addCopyRight(String copyrightText) {
		copyRight.add(copyrightText);
	}

	public RevenueInformation getOpeningWeekend() {
		return getOpeningWeekendForCountry(DEFAULT_COUNTRY);
	}

	public RevenueInformation getOpeningWeekendForCountry(String countryName) {
		return openingWeekendByCountry.get(countryName);
	}

	public void setOpeningWeekend(RevenueInformation openingWeekend) {
		openingWeekendByCountry
				.put(openingWeekend.getCountry(), openingWeekend);
	}

	public void addAdmissions(AdmissionInformation admissions) {
		addInformationDetail(admissions, totalAdmissionsByCountry);
	}

	private static <T extends BusinessInformationDetail> void addInformationDetail(
			T informationDetail, Map<String, List<T>> lookupTable) {
		getOrCreateCountryInformation(informationDetail.getCountry(),
				lookupTable).add(informationDetail);
	}

	private static <T extends BusinessInformationDetail> List<T> getOrCreateCountryInformation(
			String countryName, Map<String, List<T>> lookupTable) {
		List<T> entry = lookupTable.get(countryName);
		if (entry == null) {
			entry = new LinkedList<>();
			lookupTable.put(countryName, entry);
		}
		return entry;
	}

	public List<AdmissionInformation> getAdmissionsForCountry(String countryName) {
		return totalAdmissionsByCountry.get(countryName);
	};

	public AdmissionInformation getMaxAdmissionsOfAllCountries() {
		Optional<AdmissionInformation> outerMax = totalAdmissionsByCountry
				.values()
				.stream()
				.map(admissionsForCountry -> admissionsForCountry.stream().max(
						AdmissionInformation.maxCountComparator()))
				.filter(adm -> ((Optional<AdmissionInformation>) adm)
						.isPresent()).map(adm -> adm.get())
				.max(AdmissionInformation.maxCountComparator());
		return outerMax.orElse(null);
	}

	public int getAllCountryAdmissions() {
		return totalAdmissionsByCountry
				.values()
				.stream()
				.map(admissionsForCountry -> admissionsForCountry.stream().max(
						AdmissionInformation.maxCountComparator()))
				.collect(
						Collectors.summingInt(adm -> adm.isPresent() ? adm
								.get().getCount() : 0));
	}

	public AdmissionInformation getMaxAdmissions(String countryName,
			Comparator<AdmissionInformation> comparator) {
		return getAdmissionsForCountry(countryName).stream().max(comparator)
				.orElse(null);
	}

	public AdmissionInformation getMaxAdmissions(
			Comparator<AdmissionInformation> comparator) {
		return getMaxAdmissions(DEFAULT_COUNTRY, comparator);
	}

	public void addWeeklyGross(RevenueInformation weeklyGross) {
		addInformationDetail(weeklyGross, weeklyGrossByCountry);
	}

	public List<RevenueInformation> getWeeklyGrossForCountry(String countryName) {
		return weeklyGrossByCountry.get(countryName);
	}

	public RevenueInformation getMaxWeeklyGrossForCountry(String countryName,
			Comparator<RevenueInformation> comparator) {
		return determineMaxRevenue(comparator,
				getWeeklyGrossForCountry(countryName));
	}

	private RevenueInformation determineMaxRevenue(
			Comparator<RevenueInformation> comparator,
			List<RevenueInformation> revenueByCountry) {
		if (revenueByCountry == null) {
			return null;
		}
		return revenueByCountry.stream().max(comparator).orElse(null);
	}

	public void addGrossRevenue(RevenueInformation grossRevenue) {
		addInformationDetail(grossRevenue, grossRevenueByCountry);
	}

	public List<RevenueInformation> getGrossRevenueByCountry(String countryName) {
		return grossRevenueByCountry.get(countryName);
	}

	public RevenueInformation getMaxGrossRevenueInformationByCountry(
			String countryName, Comparator<RevenueInformation> comparator) {
		return determineMaxRevenue(comparator,
				getGrossRevenueByCountry(countryName));
	}

	public RevenueInformation getGrossRevenue() {
		return getMaxGrossRevenueInformationByCountry(DEFAULT_COUNTRY, RevenueInformation.maxRevenueComparator());
	}
}
