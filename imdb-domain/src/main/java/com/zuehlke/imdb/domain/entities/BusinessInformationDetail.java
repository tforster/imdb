package com.zuehlke.imdb.domain.entities;

import java.time.temporal.TemporalAccessor;

public abstract class BusinessInformationDetail {

	private String country;
	private TemporalAccessor date;

	public String getCountry() {
		return country;
	}
	
	public void setCountry(String country) {
		this.country = country;
	}

	public TemporalAccessor getDate() {
		return date;
	}
	
	public void setDate(TemporalAccessor date) {
		this.date = date;
	}

}
