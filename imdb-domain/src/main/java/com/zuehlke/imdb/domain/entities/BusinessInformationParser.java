package com.zuehlke.imdb.domain.entities;

import static java.time.temporal.ChronoField.DAY_OF_MONTH;
import static java.time.temporal.ChronoField.MONTH_OF_YEAR;
import static java.time.temporal.ChronoField.YEAR;

import java.time.LocalDate;
import java.time.Month;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.SignStyle;
import java.time.temporal.TemporalAccessor;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class BusinessInformationParser {
    private static final Logger log = LoggerFactory.getLogger(BusinessInformationParser.class);


	private static final String BUDGET_MARKER = "BT:";
	private static final String COPYRIGHT_MARKER = "CP:";
	private static final String ADMISSIONS_MARKER = "AD:";
	private static final String OPENING_WEEKEND_MARKER = "OW:";
	private static final String GROSS_REVENUE_MARKER = "GR:";
	private static final String WEEKLY_GROSS_REVENUE_MARKER = "WG:";

	private static final Pattern admissionEntryStructure = Pattern
			.compile("([0-9,.]+)\\s+\\(([^\\)]+)\\)(?:\\s+\\(([^\\)]*\\d{4})\\))?(?:\\s+\\((.*)\\s+screens?\\))?.*?");
	private static final Pattern revenueEntryStructure = Pattern
			.compile("(\\w+)\\s+([0-9,.]+)\\s+\\(([^\\)]+)\\).*?(?:\\s+\\(([^\\)]*\\d{4})\\))?.*?(?:\\s+\\((.*)\\s+screens?\\))?");
	private static final DateTimeFormatter dateFormatter = getDateFormatter();

	public BusinessInformation parse(String businessText) {
		BusinessInformation currentInfo = new BusinessInformation();

		if (businessText != null) {
			String[] splittedInformation = businessText.split("\\n");

			for (String splittedInfo : splittedInformation) {

				handleCopyRight(currentInfo, splittedInfo);

				handleBudget(currentInfo, splittedInfo, businessText);
				handleProductionPeriods(currentInfo, splittedInfo, businessText);
				handleAdmissions(currentInfo, splittedInfo, businessText);
				handleRevenues(currentInfo, splittedInfo, businessText);
			}
		}
		return currentInfo;
	}

	private static void handleCopyRight(BusinessInformation currentInfo,
			String line) {
		if (line.startsWith(COPYRIGHT_MARKER)) {
			currentInfo.addCopyRight(line.substring(COPYRIGHT_MARKER.length())
					.trim());
		}
	}

	private static void handleProductionPeriods(
			BusinessInformation currentInfo, String line, String businssText) {
		if (line.startsWith("PD:") || line.startsWith("SD:")) {
			String[] parts = line.substring(3).trim().split("-");
			try {
				ProductionPeriod productionPeriod = new ProductionPeriod();
				if (parts.length > 0) {
					productionPeriod.setProductionStart(parseDate(parts[0],
							dateFormatter));
					if (parts.length > 1) {
						productionPeriod.setProductionEnd(parseDate(parts[1],
								dateFormatter));
					}
					currentInfo.setProductionPeriod(productionPeriod);
				}
			} catch (Exception e) {
				logErrorWhileParsing("production period", currentInfo,
						businssText, line, e);
			}

		}
	}

	private static void handleBudget(BusinessInformation currentInfo,
			String line, String businessText) {
		if (line.startsWith(BUDGET_MARKER)) {
			String[] parts = line.substring(BUDGET_MARKER.length()).trim()
					.split(" ");
			Money budget = new Money();
			budget.setCurrency(parts[0]);
			try {
				budget.setValue(Long.parseLong(parts[1].replaceAll("[,.]", "")));
				currentInfo.setProductionBudget(budget);
			} catch (NumberFormatException e) {
				logErrorWhileParsing("budget", currentInfo, businessText, line,
						e);
			}
		}
	}

	private static void handleAdmissions(BusinessInformation currentInfo,
			String line, String businessText) {
		if (line.startsWith(ADMISSIONS_MARKER)) {
			String trimmedLine = line.substring(ADMISSIONS_MARKER.length())
					.trim();
			try {
				Matcher matcher = admissionEntryStructure.matcher(trimmedLine);
				if (matcher.matches()) {
					AdmissionInformation admission = new AdmissionInformation();
					admission.setCount(Integer.parseInt(matcher.group(1)
							.replaceAll("[,.]", "").trim()));
					admission.setCountry(matcher.group(2));
					if (matcher.groupCount() > 2) {
						admission.setDate(parseDate(matcher.group(3),
								dateFormatter));
					}
					currentInfo.addAdmissions(admission);
				} else {
					log.trace("Doesn't match: " + trimmedLine);
				}
			} catch (Exception e) {
				logErrorWhileParsing("admissions", currentInfo, businessText,
						line, e);
			}
		}
	}

	private static void handleRevenues(BusinessInformation currentInfo,
			String line, String businessText) {
		if (line.startsWith(GROSS_REVENUE_MARKER)) {
			String trimmedLine = line.substring(GROSS_REVENUE_MARKER.length())
					.trim();
			RevenueInformation revenue;
			try {
				revenue = parseRevenue(dateFormatter, revenueEntryStructure,
						trimmedLine);
				if (revenue != null)
					currentInfo.addGrossRevenue(revenue);
			} catch (Exception e) {
				logErrorWhileParsing("gross revenue", currentInfo,
						businessText, line, e);
			}
		}

		if (line.startsWith(WEEKLY_GROSS_REVENUE_MARKER)) {
			String trimmedLine = line.substring(
					WEEKLY_GROSS_REVENUE_MARKER.length()).trim();
			RevenueInformation revenue;
			try {
				revenue = parseRevenue(dateFormatter, revenueEntryStructure,
						trimmedLine);
				if (revenue != null)
					currentInfo.addWeeklyGross(revenue);
			} catch (Exception e) {
				logErrorWhileParsing("weekly gross revenue", currentInfo,
						businessText, line, e);
			}
		}

		if (line.startsWith(OPENING_WEEKEND_MARKER)) {
			String trimmedLine = line
					.substring(OPENING_WEEKEND_MARKER.length()).trim();
			RevenueInformation revenue;
			try {
				revenue = parseRevenue(dateFormatter, revenueEntryStructure,
						trimmedLine);
				if (revenue != null)
					currentInfo.setOpeningWeekend(revenue);
			} catch (Exception e) {
				logErrorWhileParsing("opening weekend", currentInfo,
						businessText, line, e);
			}
		}
	}

	private static RevenueInformation parseRevenue(
			DateTimeFormatter dateFormatter, Pattern revenueEntryStructure,
			String trimmedLine) {
		Matcher matcher = revenueEntryStructure.matcher(trimmedLine);
		if (matcher.matches()) {
			RevenueInformation result = new RevenueInformation();
			Money money = new Money();
			money.setCurrency(matcher.group(1));
			money.setValue(Long.parseLong(matcher.group(2)
					.replaceAll("[,.]", "").trim()));
			result.setMoney(money);
			result.setCountry(matcher.group(3));
			result.setDate(parseDate(matcher.group(4), dateFormatter));
			if (matcher.groupCount() > 4 && matcher.group(5) != null) {
				result.setScreens(Integer.parseInt(matcher.group(5)
						.replaceAll("[,.]", "").trim()));
			}
			return result;
		} else {
			log.trace("Doesn't match: " + trimmedLine);
			return null;
		}
	}

	private static void logErrorWhileParsing(String parsingSubject,
			BusinessInformation currentInfo, String businessText, String line,
			Exception e) {
		log.trace("Couldn't parse " + parsingSubject + " on line " + ":" + line);
		log.trace("Full context: " + businessText, e);
	}

	private static TemporalAccessor parseDate(String string,
			DateTimeFormatter dateFormatter) {
		if (string == null)
			return null;

		String trimmedString = string.trim();
		int indexOf = trimmedString.indexOf('(');
		if (indexOf >= 0) {
			trimmedString = trimmedString.substring(0, indexOf).trim();
		}

		if (trimmedString.isEmpty() || trimmedString.contains("?"))
			return null;

		if (trimmedString.contains(" ")) {
			return dateFormatter.parse(trimmedString);
		} else {
			return LocalDate.of(Integer.parseInt(trimmedString), 1, 1);
		}
	}

	private static DateTimeFormatter getDateFormatter() {
		Map<Long, String> moy = new HashMap<>(Month.values().length);
		for (Month m : Month.values()) {
			moy.put(Long.valueOf(m.getValue()), m.name());
		}

		return new DateTimeFormatterBuilder().parseCaseInsensitive()
				.parseLenient().optionalStart()
				.appendValue(DAY_OF_MONTH, 1, 2, SignStyle.NOT_NEGATIVE)
				.optionalEnd().padNext(20).appendText(MONTH_OF_YEAR, moy)
				.padNext(20).appendValue(YEAR, 1, 4, SignStyle.NORMAL)
				.toFormatter();

	}

}
