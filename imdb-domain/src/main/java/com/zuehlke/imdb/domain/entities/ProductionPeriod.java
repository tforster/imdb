package com.zuehlke.imdb.domain.entities;

import java.time.LocalDate;
import java.time.Period;
import java.time.temporal.TemporalAccessor;

public class ProductionPeriod {
	TemporalAccessor productionStart;
	TemporalAccessor productionEnd;

	public TemporalAccessor getProductionStart() {
		return productionStart;
	}

	public void setProductionStart(TemporalAccessor productionStart) {
		this.productionStart = productionStart;
	}

	public TemporalAccessor getProductionEnd() {
		return productionEnd;
	}

	public void setProductionEnd(TemporalAccessor productionEnd) {
		this.productionEnd = productionEnd;
	}

	public Period asPeriod() {
		try {
			LocalDate start = LocalDate.from(productionStart);
			LocalDate end = LocalDate.from(productionEnd);
			return Period.between(start, end);
		} catch (Exception e) {
			return null;
		}
	}
}