package com.zuehlke.imdb.domain.entities;

import static javax.persistence.GenerationType.*;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.zuehlke.imdb.domain.Actors;
import com.zuehlke.imdb.domain.Genres;

@Entity
@Table(name = "movies", catalog = "imdb")
public class Movie implements Serializable {

	private static final long serialVersionUID = -7120629258226692631L;

	private Integer movieid;
	private String imdbid;
	private String title;
	private double ranking;
	private int votes;
	private String year;
	private double weightedScore;
	private Collection<Actors> actors;
	private Collection<Genres> genres;
	private BusinessInformation business;

	public Movie(){
		
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "movieid", unique = true, nullable = false)
	public Integer getMovieid() {
		return this.movieid;
	}

	public void setMovieid(Integer movieid) {
		this.movieid = movieid;
	}

	@Column(name = "title", nullable = false, length = 400)
	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Transient
	public double getRanking() {
		return ranking;
	}

	public void setRanking(double rank) {
		this.ranking = rank;
	}

	@Transient
	public int getVotes() {
		return votes;
	}

	public void setVotes(int votes) {
		this.votes = votes;
	}
	
	@Transient
	public double getWeightedScore() {
		return weightedScore;
	}

	public void setWeightedScore(double weightedScore) {
		this.weightedScore = weightedScore;
	}
	
	@Column(name = "year", length = 100)
	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Column(name = "imdbid", length = 10)
	public String getImdbid() {
		return this.imdbid;
	}

	public void setImdbid(String imdbid) {
		this.imdbid = imdbid;
	}

	@ManyToMany
	@JoinTable(name = "movies2actors", joinColumns = @JoinColumn(name = "movieid"), inverseJoinColumns = @JoinColumn(name = "actorid"))
	public Collection<Actors> getActors() {
		return actors;
	}

	public void setActors(Collection<Actors> actors) {
		this.actors = actors;
	}

	@OneToMany
	@JoinColumn(name = "movieid")
	public Collection<Genres> getGenres() {
		return genres;
	}

	public void setGenres(Collection<Genres> genres) {
		this.genres = genres;
	}
	
	@Transient
	public BusinessInformation getBusiness() {
		return business;
	}
	
	public void setBusiness(BusinessInformation business) {
		this.business = business;
	}
}