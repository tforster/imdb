package com.zuehlke.imdb.domain;

// Generated Jun 2, 2014 1:54:53 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Language generated by hbm2java
 */
@Entity
@Table(name = "language", catalog = "imdb")
public class Language implements java.io.Serializable {

	private static final long serialVersionUID = 6765942727163235126L;

	private LanguageId id;

	public Language() {
	}

	public Language(LanguageId id) {
		this.id = id;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "movieid", column = @Column(name = "movieid", nullable = false)),
			@AttributeOverride(name = "language", column = @Column(name = "language", nullable = false, length = 100)),
			@AttributeOverride(name = "addition", column = @Column(name = "addition", length = 1000)) })
	public LanguageId getId() {
		return this.id;
	}

	public void setId(LanguageId id) {
		this.id = id;
	}

}
