package com.zuehlke.imdb.domain;

// Generated Jun 2, 2014 1:54:53 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Certificates generated by hbm2java
 */
@Entity
@Table(name = "certificates", catalog = "imdb")
public class Certificates implements java.io.Serializable {

	private static final long serialVersionUID = 2367375947200454720L;

	private CertificatesId id;

	public Certificates() {
	}

	public Certificates(CertificatesId id) {
		this.id = id;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "movieid", column = @Column(name = "movieid", nullable = false)),
			@AttributeOverride(name = "country", column = @Column(name = "country", length = 50)),
			@AttributeOverride(name = "certification", column = @Column(name = "certification", nullable = false)),
			@AttributeOverride(name = "addition", column = @Column(name = "addition", length = 1000)) })
	public CertificatesId getId() {
		return this.id;
	}

	public void setId(CertificatesId id) {
		this.id = id;
	}

}
