package com.zuehlke.imdb.domain;

// Generated Jun 2, 2014 1:54:53 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Movies generated by hbm2java
 */
@Entity
@Table(name = "movies", catalog = "imdb")
public class Movies implements java.io.Serializable {

	private static final long serialVersionUID = 2667017808758234428L;

	private Integer movieid;
	private String title;
	private String year;
	private String imdbid;

	public Movies() {
	}

	public Movies(String title) {
		this.title = title;
	}

	public Movies(String title, String year, String imdbid) {
		this.title = title;
		this.year = year;
		this.imdbid = imdbid;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "movieid", unique = true, nullable = false)
	public Integer getMovieid() {
		return this.movieid;
	}

	public void setMovieid(Integer movieid) {
		this.movieid = movieid;
	}

	@Column(name = "title", nullable = false, length = 400)
	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	@Column(name = "year", length = 100)
	public String getYear() {
		return this.year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	@Column(name = "imdbid", length = 10)
	public String getImdbid() {
		return this.imdbid;
	}

	public void setImdbid(String imdbid) {
		this.imdbid = imdbid;
	}

}
