package com.zuehlke.imdb.domain;

// Generated Jun 2, 2014 1:54:53 PM by Hibernate Tools 3.4.0.CR1

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Plots generated by hbm2java
 */
@Entity
@Table(name = "plots", catalog = "imdb")
public class Plots implements java.io.Serializable {

	private static final long serialVersionUID = 5755522224524485775L;

	private PlotsId id;

	public Plots() {
	}

	public Plots(PlotsId id) {
		this.id = id;
	}

	@EmbeddedId
	@AttributeOverrides({
			@AttributeOverride(name = "movieid", column = @Column(name = "movieid", nullable = false)),
			@AttributeOverride(name = "plottext", column = @Column(name = "plottext", length = 65535)) })
	public PlotsId getId() {
		return this.id;
	}

	public void setId(PlotsId id) {
		this.id = id;
	}

}
