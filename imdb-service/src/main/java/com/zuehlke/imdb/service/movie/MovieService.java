package com.zuehlke.imdb.service.movie;

import java.util.List;

import com.zuehlke.imdb.domain.entities.Movie;

public interface MovieService {

	Movie getMovie(int id);
	
	List<Movie> getMoviesByName(String movieName);
	
	List<Movie> getAllMovies();
}
