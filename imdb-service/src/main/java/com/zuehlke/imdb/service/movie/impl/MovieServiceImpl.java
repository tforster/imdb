package com.zuehlke.imdb.service.movie.impl;

import java.text.MessageFormat;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.StringUtils;

import com.zuehlke.imdb.domain.entities.Movie;
import com.zuehlke.imdb.service.movie.MovieService;

public class MovieServiceImpl implements MovieService {

	private EntityManager entityManager;
	
	public MovieServiceImpl(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	public List<Movie> getAllMovies() {
		TypedQuery<Movie> query = entityManager.createQuery("select m from Movie m", Movie.class);
		return query.getResultList();
	}
	
	@Override
	public Movie getMovie(int id) {
		TypedQuery<Movie> query = entityManager.createQuery("select m from Movie m where m.movieid = :movieid", Movie.class);
		query.setParameter("movieid", id);
		
		List<Movie> results = query.getResultList();
		if (results.size() != 1) {
			throw new RuntimeException(MessageFormat.format("Found more than one movie for id ''{0}''!", results.size()));
		}
		
		return results.get(0);
	}
	
	@Override
	public List<Movie> getMoviesByName(String movieName) {
		StringBuilder queryBuilder = new StringBuilder();
		queryBuilder.append("select m from Movie m");
		
		if (StringUtils.isNotBlank(movieName)) {
			queryBuilder.append(" where m.title like :title");
		}
		
		TypedQuery<Movie> query = entityManager.createQuery(queryBuilder.toString(), Movie.class);
		if (StringUtils.isNotBlank(movieName)) {
			query.setParameter("title", movieName.replaceAll("\\*", "%"));
		}
		
		return query.getResultList();
	}
}
