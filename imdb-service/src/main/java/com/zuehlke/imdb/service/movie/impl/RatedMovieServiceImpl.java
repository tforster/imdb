package com.zuehlke.imdb.service.movie.impl;

import static com.zuehlke.imdb.service.utils.Constants.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang3.Range;

import com.zuehlke.imdb.domain.entities.BusinessInformationParser;
import com.zuehlke.imdb.domain.entities.Movie;
import com.zuehlke.imdb.service.movie.RatedMovieService;

public class RatedMovieServiceImpl implements RatedMovieService {

	private static final String SELECT_MOVIE_WITH_RANK_AND_BUSINESS = "SELECT m.title, r.rank, r.votes, m.year, b.businesstext FROM ratings r";
	private static final String JOIN_MOVIES = " JOIN movies m ON m.movieid = r.movieid";
	private static final String OUTER_JOIN_BUSINESS = " LEFT OUTER JOIN business b ON m.movieid = b.movieid";
	private EntityManager entityManager;
	private final BusinessInformationParser businessInformationParser = new BusinessInformationParser();
	
	private static List<Movie> cachedRatedMovies; 
	
	private String QUERY_TITLE_TO_RANK = SELECT_MOVIE_WITH_RANK_AND_BUSINESS 
			+ JOIN_MOVIES 
			+ OUTER_JOIN_BUSINESS 
			+ " WHERE r.votes > " + VOTES_THRESHOLD
			+ " ORDER BY r.rank DESC";

	private final static String QUERY_ACTOR_BY_NAME = "SELECT a.actorid FROM Actors a WHERE a.name = :name";
	
	public RatedMovieServiceImpl(EntityManager entityManager){
		this.entityManager = entityManager; 
	}

	@Override
	public List<Movie> getRatedMovies() {
		if (cachedRatedMovies != null && !cachedRatedMovies.isEmpty()) {
			return cachedRatedMovies;
		}
		
		List<Movie> ratedMovies = new ArrayList<Movie>();
		
		try{
			Query query = entityManager.createNativeQuery(
					QUERY_TITLE_TO_RANK);
			
			@SuppressWarnings("unchecked")
			List<Object[]> results = query.getResultList();
			
			for(Object[] plain : results){
				Movie movie = convertQueryResult(plain);
				ratedMovies.add(movie);
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}

		ratedMovies = Collections.unmodifiableList(ratedMovies);
		cachedRatedMovies = ratedMovies;
		return ratedMovies;
	}
	
	public List<Movie> getRatedMoviesByActorname(String  name) {
		List<Integer> actorIds = getActorIDByName(name);
		if (actorIds.isEmpty()) {
			return Collections.emptyList();
		}
		
		List<Integer> movieIds = getMoviesIdsByActorsIds(actorIds);
		if (movieIds.isEmpty()) {
			return Collections.emptyList();
		}
		
		String query = SELECT_MOVIE_WITH_RANK_AND_BUSINESS
				+ JOIN_MOVIES 
				+ OUTER_JOIN_BUSINESS 
				+ " WHERE m.movieid in (" + movieIds.stream().map(id -> { return id.toString(); }).collect(Collectors.joining(", ")) + ")";
		
		Query nativeQuery = entityManager.createNativeQuery(query.toString());
		@SuppressWarnings("unchecked")
		List<Object[]> results = nativeQuery.getResultList();
		
		List<Movie> enrichedMovies = new ArrayList<Movie>();
		for(Object[] plain : results){
			Movie movie = convertQueryResult(plain);
			enrichedMovies.add(movie);
		}       
		        
		return enrichedMovies;
	}
	
	@Override
	public List<Movie> getRatedMovies(String movieTitle,
			Range<Double> ratingRange) {
		
		String queryString = SELECT_MOVIE_WITH_RANK_AND_BUSINESS 
				+ JOIN_MOVIES 
				+ OUTER_JOIN_BUSINESS 
				+ " WHERE r.votes > :votesThreshold";
		
		if (shouldQueryMovie(movieTitle)) {
			queryString += " AND m.title LIKE :movieTitle";
		}
		
		if (shouldQueryRatingRange(ratingRange)) {
			queryString += " AND r.rank >= :minRank"
						+ " AND r.rank <= :maxRank";
		}
		
		queryString += " ORDER BY r.rank DESC";
		
		if (!shouldQueryMovie(movieTitle) && !shouldQueryRatingRange(ratingRange)) {
			queryString += " LIMIT 200";
		}
		
		Query query = entityManager.createNativeQuery(queryString);
		if (shouldQueryRatingRange(ratingRange)) {
			query.setParameter("minRank", ratingRange.getMinimum());
			query.setParameter("maxRank", ratingRange.getMaximum());
		}
		if (shouldQueryMovie(movieTitle))
			query.setParameter("movieTitle", movieTitle.replaceAll("\\*", "%"));
		query.setParameter("votesThreshold", 0);
		
		@SuppressWarnings("unchecked")
		List<Object[]> results = query.getResultList();
		
		List<Movie> enrichedMovies = new ArrayList<Movie>();
		for(Object[] plain : results){
			Movie movie = convertQueryResult(plain);
			enrichedMovies.add(movie);
		}       
		        
		return enrichedMovies;
	}

	private Movie convertQueryResult(Object[] plain) {
		Movie movie = new Movie();
		movie.setTitle((String)plain[0]);
		movie.setRanking(Double.parseDouble((String)plain[1]));
		movie.setVotes((int)plain[2]);
		movie.setYear((String)plain[3]);
		movie.setBusiness(businessInformationParser.parse((String)plain[4]));
		return movie;
	}

	// example "Diaz, Cameron", "Clooney, George", "Zeta-Jones, Catherine"
	private List<Integer> getActorIDByName(String name) {
		TypedQuery<Integer> query = entityManager.createQuery(QUERY_ACTOR_BY_NAME, Integer.class);
		query.setParameter("name", name);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private List<Integer> getMoviesIdsByActorsIds(List<Integer> ids) {
		StringBuilder queryBuilder = new StringBuilder("SELECT movieid FROM Movies2actors WHERE actorid IN (");
		
		int counter = 0;
		int sizeActors = ids.size();
		for(Integer actorid : ids){
			queryBuilder.append(actorid);
			counter++;
			
			if(counter < sizeActors)
				queryBuilder.append(", ");
		}
		queryBuilder.append(")");
		String queryMovies = queryBuilder.toString();
		
		Query query = entityManager.createNativeQuery(
				queryMovies);
		
		return query.getResultList();
	}
	
	private boolean shouldQueryRatingRange(Range<Double> ratingRange) {
		return ratingRange != null && ratingRange.getMinimum() != null && ratingRange.getMaximum() != null && ratingRange.getMinimum() > 1.0 || ratingRange.getMaximum() < 10.0;
	}

	private boolean shouldQueryMovie(String movieTitle) {
		return movieTitle != null && !movieTitle.trim().isEmpty();
	}
}
