package com.zuehlke.imdb.service.processors;

import static com.zuehlke.imdb.service.utils.Constants.MIN_VOTES_REQUIRED;
import static com.zuehlke.imdb.service.utils.Constants.AVG_RANK;

public class WeightedScoreComputor {

	public static double calculate(double rating, int votes){
		return ((double)votes / (double)(votes + MIN_VOTES_REQUIRED)) * rating + 
				((double)MIN_VOTES_REQUIRED / (double)(votes + MIN_VOTES_REQUIRED)) * AVG_RANK;
	}
}