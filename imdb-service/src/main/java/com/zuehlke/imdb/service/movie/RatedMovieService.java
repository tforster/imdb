package com.zuehlke.imdb.service.movie;

import java.util.List;

import org.apache.commons.lang3.Range;

import com.zuehlke.imdb.domain.entities.Movie;


public interface RatedMovieService {

	List<Movie> getRatedMovies(String movieTitle, Range<Double> ratingRange);
	
	List<Movie> getRatedMovies();
	
	List<Movie> getRatedMoviesByActorname(String  name);
}
