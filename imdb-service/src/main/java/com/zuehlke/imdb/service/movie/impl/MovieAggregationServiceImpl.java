package com.zuehlke.imdb.service.movie.impl;

import java.sql.SQLException;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;

import com.zuehlke.imdb.domain.entities.Movie;
import com.zuehlke.imdb.service.movie.MovieAggregationService;
import com.zuehlke.imdb.service.movie.RatedMovieService;
import com.zuehlke.imdb.service.processors.WeightedScoreComputor;

public class MovieAggregationServiceImpl implements MovieAggregationService{
	private RatedMovieService ratedMovieService;
	
	public MovieAggregationServiceImpl(EntityManager entityManager){
		this.ratedMovieService = new RatedMovieServiceImpl(entityManager);
	}
	
	public List<Movie> getMoviesWithWeightedScoresByActorname(String name) {
		List<Movie> movies = ratedMovieService.getRatedMoviesByActorname(name);
		movies = normalizeNames(movies);
		movies = aggregateEntriesWithEqualTitles(movies);
		movies = setWeightedScores(movies);
		return sortRatedMovies(movies);
	}
	
	public List<Movie> getMoviesWithWeightedScores() throws SQLException{
		List<Movie> movies = ratedMovieService.getRatedMovies();
		movies = normalizeNames(movies);
		movies = aggregateEntriesWithEqualTitles(movies);
		movies = setWeightedScores(movies);
		return sortRatedMovies(movies);
	}
	
	public List<Movie> getMoviesWithWeightedScoreByMoviename(String name){
		List<Movie> movies = ratedMovieService.getRatedMovies();
		movies = normalizeNames(movies);
		movies = aggregateEntriesWithEqualTitles(movies);
		movies = filterMoviesByNamePrefix(movies, name);
		movies = setWeightedScores(movies);
		return sortRatedMovies(movies);		
	}
	
	/*
	 * remove title extension in curly brackets
	 */
	private List<Movie> normalizeNames(List<Movie> movies){
		
		movies.stream().
			forEach(m -> m.setTitle(removeTitleExtention(m.getTitle())));
		
		return movies;
	}
	
	private List<Movie> aggregateEntriesWithEqualTitles(List<Movie> movies){
		List<Movie> result = new LinkedList<Movie>();

		// group by title
		Map<String, List<Movie>> mapByTitle = movies.stream().collect(Collectors.groupingBy(Movie::getTitle));

		// aggregate rank and votes
		for(List<Movie> l : mapByTitle.values()){
			Movie movie = new Movie();
			movie.setTitle(l.get(0).getTitle());
			movie.setRanking(l.stream().mapToDouble(Movie::getRanking).average().getAsDouble());
			movie.setVotes(l.stream().mapToInt(Movie::getVotes).sum());
			movie.setYear(l.stream().findFirst().get().getYear());
			
			result.add(movie);
		}

		return result;
	}
	
	private List<Movie> filterMoviesByNamePrefix(List<Movie> movies, String name){
		return movies.stream().filter(m -> m.getTitle().contains(name)).collect(Collectors.toList());
	}
	
	private List<Movie> sortRatedMovies(List<Movie> movies){
		movies.sort(Comparator.comparing(m -> - (Double)m.getWeightedScore()));		
		return movies;
	}
	
	private List<Movie> setWeightedScores(List<Movie> movies){
		movies.stream().forEach(m -> m.setWeightedScore(WeightedScoreComputor.calculate(m.getRanking(), m.getVotes())));
		return movies;
	}
	
	/*
	 * remove title extension of type "Breaking Bad" (2008) {Face Off (#4.13)}
	 * and return it in the form "Breaking Bad" (2008)
	 */
	private String removeTitleExtention(String title){
		if(title.matches(".*[{]{1}.*[}]{1}")){
			int idxClosingBracket = title.lastIndexOf("}");
			int idxOpeningBracket = title.substring(0, idxClosingBracket).lastIndexOf("{");
			return title.substring(0, idxOpeningBracket).trim();
		}
		else{
			return title;
		}
	}
}
