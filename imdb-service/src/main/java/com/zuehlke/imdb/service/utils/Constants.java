package com.zuehlke.imdb.service.utils;
public class Constants {

	public final static int VOTES_THRESHOLD = 1000;
	public final static int SHOW_LIMIT = 300;
	public static final int MIN_VOTES_REQUIRED = 3000;
	public static final double AVG_RANK = 6.7;
}