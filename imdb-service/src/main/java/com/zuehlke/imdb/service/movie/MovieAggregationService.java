package com.zuehlke.imdb.service.movie;

import java.sql.SQLException;
import java.util.List;

import com.zuehlke.imdb.domain.entities.Movie;

public interface MovieAggregationService {

	List<Movie> getMoviesWithWeightedScores() throws SQLException;
	
	List<Movie> getMoviesWithWeightedScoreByMoviename(String name);
	
	List<Movie> getMoviesWithWeightedScoresByActorname(String name);
}
